package com.example.challengechap5.model


import com.google.gson.annotations.SerializedName

data class RequestUser(
    @SerializedName("alamat")
    val alamat: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("tanggal")
    val tanggal: String,
    @SerializedName("username")
    val username: String
)