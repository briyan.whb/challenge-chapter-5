package com.example.challengechap5.fragment

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.challengechap5.R
import com.example.challengechap5.api.ApiUserClient
import com.example.challengechap5.databinding.FragmentUpdateBinding
import com.example.challengechap5.model.GetAllUsersResponseItem
import com.example.challengechap5.model.RequestUser
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UpdateFragment : Fragment() {

    private var _binding : FragmentUpdateBinding? = null
    private val binding get() = _binding!!
    private lateinit var shared : SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUpdateBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shared = requireActivity().getSharedPreferences("onBoard", Context.MODE_PRIVATE)

        val nama : String = shared.getString("NAMA_KEY", "").toString()
        val tanggal : String = shared.getString("TANGGAL_KEY", "").toString()
        val alamat : String = shared.getString("ALAMAT_KEY", "").toString()
        val user : String = shared.getString("USER_KEY", "").toString()
        val email : String = shared.getString("EMAIL_KEY", "").toString()
        val id = shared.getString("ID_KEY", "").toString()

        binding.edNama.setText(nama)
        binding.edTanggal.setText(tanggal)
        binding.edAlamat.setText(alamat)
        binding.edUser.setText(user)
        binding.edEmail.setText(email)

        binding.logout.setOnClickListener {
            logout(view)
        }

        binding.update.setOnClickListener {
            val namax = binding.edNama.text.toString()
            val tanggalx = binding.edTanggal.text.toString()
            val alamatx = binding.edAlamat.text.toString()
            val username = binding.edUser.text.toString()
            val emailx = binding.edEmail.text.toString()
            val pass = binding.edPass.text.toString()
            val passX = binding.edPassX.text.toString()
            shared = requireActivity().getSharedPreferences("onBoard",
                Context.MODE_PRIVATE)
            val sharedEditor = shared.edit()
            sharedEditor.apply {
                putString("USER_KEY", username)
                putString("EMAIL_KEY", emailx)
                putString("NAMA_KEY", namax)
                putString("ID_KEY", id)
                putString("ALAMAT_KEY", alamatx)
                putString("TANGGAL_KEY", tanggalx)
                apply()
            }
            if (pass.isNotBlank()) {
                if (pass == passX) {
                    updateUserData(id, namax, tanggalx, alamatx, username, emailx, pass, view)
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Password dan konfirmasi password harus sama",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun updateUserData(
        id: String,
        nama: String,
        tanggal: String,
        alamat: String,
        username: String,
        email: String,
        password: String,
        view: View
    ) {AlertDialog.Builder(requireContext())
        .setTitle("UPDATE")
        .setMessage("Yakin ingin mengupdate data anda?")
        .setNegativeButton("Tidak"){ dialogInterface: DialogInterface, i: Int ->
            dialogInterface.dismiss()
        }.setPositiveButton("Ya"){ dialogInterface: DialogInterface, i: Int ->
            shared = requireActivity().getSharedPreferences("LOGGED_IN", Context.MODE_PRIVATE)
            ApiUserClient.instance.updateUser(id, RequestUser(
                alamat,
                email,
                nama,
                password,
                tanggal,
                username
            )).enqueue(object : Callback<RequestUser>{
                override fun onResponse(
                    call: Call<RequestUser>,
                    response: Response<RequestUser>
                ) {
                    Toast.makeText(requireContext(), "Data Telah Diupdate", Toast.LENGTH_LONG).show()
                    Navigation.findNavController(view).navigateUp()
                }
                override fun onFailure(call: Call<RequestUser>, t: Throwable) {
                }

            })
        }.show()
    }

    private fun logout(view: View) {
        AlertDialog.Builder(requireContext())
            .setTitle("LOGOUT")
            .setMessage("Yakin ingin logout?")
            .setNegativeButton("Tidak"){ dialogInterface: DialogInterface, i: Int ->
                dialogInterface.dismiss()
            }.setPositiveButton("Ya"){ dialogInterface: DialogInterface, i: Int ->
                onBoarding()
                Toast.makeText(requireContext(), "Anda Telah Logout", Toast.LENGTH_LONG).show()
                Navigation.findNavController(view).navigate(R.id.action_updateFragment_to_loginFragment)
            }.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun onBoarding(){
        val sharedPref = requireActivity().getSharedPreferences("onBoard", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putBoolean("Finished", false)
        editor.apply()
    }
}