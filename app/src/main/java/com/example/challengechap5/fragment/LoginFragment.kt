package com.example.challengechap5.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.challengechap5.R
import com.example.challengechap5.databinding.FragmentLoginBinding
import com.example.challengechap5.model.GetAllUsersResponseItem
import com.example.challengechap5.viewmodel.ViewModelUser

class LoginFragment : Fragment() {

    private var _binding : FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel : ViewModelUser
    private lateinit var listUser : List<GetAllUsersResponseItem>
    private lateinit var shared : SharedPreferences


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.login.setOnClickListener{
            if(binding.edEmail.text!!.isNotEmpty() && binding.edPass.text!!.isNotEmpty()){
                getDataUserUsingViewModel(view)
            } else{
                Toast.makeText(requireContext(), "Semua field harus diisi",
                    Toast.LENGTH_SHORT).show()
            }
        }

        binding.register.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_registFragment)
        }
    }

    private fun getDataUserUsingViewModel(view: View) {
        viewModel = ViewModelProvider(this)[ViewModelUser::class.java]
        viewModel.getLiveUserObserver().observe(viewLifecycleOwner) {
            listUser = it
            loginAuth(listUser, view)
        }
        viewModel.setLiveDataUserFromApi()
    }

    private fun loginAuth(user: List<GetAllUsersResponseItem>, view: View) {

        val inputEmail = binding.edEmail.text.toString()
        val inputPassword = binding.edPass.text.toString()

        for(i in user.indices){
            if(inputPassword == user[i].password && inputEmail == user[i].email){
                Toast.makeText(requireContext(), "Berhasil login", Toast.LENGTH_SHORT).show()
                navigationBundling(user[i], view)
                break
            }
            else if(i == user.lastIndex && inputPassword != user[i].password &&
                inputEmail != user[i].email){
                Toast.makeText(requireContext(), "Gagal login", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun navigationBundling(responseItem: GetAllUsersResponseItem, view: View) {
        shared = requireActivity().getSharedPreferences("onBoard",
            Context.MODE_PRIVATE)
        val sharedEditor = shared.edit()
        sharedEditor.apply {
            putString("USER_KEY", responseItem.username)
            putString("EMAIL_KEY", responseItem.email)
            putString("NAMA_KEY", responseItem.name)
            putString("ID_KEY", responseItem.id)
            putString("ALAMAT_KEY", responseItem.alamat)
            putString("TANGGAL_KEY", responseItem.tanggal)
            apply()
        }
        onBoarding()
        Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_homeFragment)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun onBoarding() {
        shared = requireActivity()
            .getSharedPreferences("onBoard", Context.MODE_PRIVATE)
        val editor = shared.edit()
        editor.putBoolean("Finished", true)
        editor.apply()
    }
}