package com.example.challengechap5.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.example.challengechap5.adapter.CardAdapter
import com.example.challengechap5.api.TMDBClient
import com.example.challengechap5.api.TMDBService
import com.example.challengechap5.databinding.FragmentHomeBinding
import com.example.challengechap5.model.Result
import com.example.challengechap5.viewmodel.MoviesViewModel
import com.example.challengechap5.viewmodel.viewModelsFactory

@SuppressLint("SetTextI18n")
class HomeFragment : Fragment() {

    private var _binding : FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private lateinit var moviesAdapter : CardAdapter
    private val apiTMDBService : TMDBService by lazy { TMDBClient.instance }
    private val viewModel : MoviesViewModel by viewModelsFactory { MoviesViewModel(apiTMDBService) }
    private lateinit var shared : SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        shared = requireActivity()
            .getSharedPreferences("onBoard", Context.MODE_PRIVATE)
        val user = shared.getString("USER_KEY", "")

        binding.textView2.text = "Welcome $user"
        initRecycler(view)
        viewModel.getAllMovies()
        getDataFromNetwork()
        binding.logout.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToUpdateFragment()
            Navigation.findNavController(view).navigate(action)
        }
    }

    private fun initRecycler(view: View){
        moviesAdapter = CardAdapter(object : CardAdapter.OnClickListener {
            override fun onClickItem(data: Result) {
                val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(data)
                Navigation.findNavController(view).navigate(action)
            }
        })
        binding.apply {
            recyclerView.adapter = moviesAdapter
            recyclerView.layoutManager = GridLayoutManager(requireContext(),2)
        }
    }

    private fun getDataFromNetwork(){
        viewModel.dataMovies.observe(viewLifecycleOwner){
            moviesAdapter.updateData(it.results)
            binding.loading.isVisible = false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}